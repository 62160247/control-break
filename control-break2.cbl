       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CONTROL-BREAK2.
       AUTHOR. JARUPHONG.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT INPUT-FILE ASSIGN TO "input2.txt"
           ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  INPUT-FILE.
       01  INPUT-BUFFER.
           88 END-OF-INPUT-FILE VALUE HIGH-VALUE .
           05 COL-A PIC X(2).
           05 COL-B PIC X(2).
           05 COL-COUNT PIC 9(3).
       WORKING-STORAGE SECTION.        
       01  TOTAL PIC 9(4) VALUE ZERO .
       01  COL-A-TOTAL PIC 9(4).
       01  COL-A-PROCESSING PIC X(2).
       01  COL-B-TOTAL PIC 9(4).
       01  COL-B-PROCESSING PIC X(2).
       01  RPT-HEADER.
           05 FILLER  PIC X(4) VALUE "  A".
           05 FILLER PIC X(4) VALUE SPACE .
           05 FILLER  PIC X(4) VALUE "   B".
           05 FILLER PIC X(4) VALUE SPACE .
           05 FILLER PIC X(5) VALUE "TOTAL".
       01  RPT-ROW.
           05 RPT-COL-A PIC BBX(2).
           05 FILLER PIC X(5) VALUE SPACE .
           05 RPT-COL-B PIC BBX(2).
           05 FILLER PIC X(5) VALUE SPACE .
           05 RPT-COL-TOTAL PIC ZZZ9.
       01  RPT-A-TOTAL-ROW.
           05 FILLER PIC X(8) VALUE "        ".             
           05 FILLER PIC x(11) VALUE "    TOTAL: ".
           05 RPT-A-TOTAL PIC ZZZ9.
       01  RPT-FOOTER.           
           05 FILLER PIC X(9) VALUE "TOTAL:" .
           05 RPT-TOTAL PIC ZZZ9.
       PROCEDURE DIVISION .
       BEGIN.
           OPEN INPUT  INPUT-FILE 
           DISPLAY RPT-HEADER 
           PERFORM READ-LINE           
           PERFORM PROCESS-COL-A    UNTIL END-OF-INPUT-FILE 
           MOVE TOTAL TO RPT-TOTAL
           DISPLAY RPT-FOOTER 
           GOBACK 
           .
       PROCESS-COL-A.
           MOVE COL-A TO COL-A-PROCESSING
           MOVE  COL-A-PROCESSING TO RPT-COL-A
           MOVE ZERO TO COL-A-TOTAL
           PERFORM PROCESS-COL-B  UNTIL COL-A NOT = COL-A-PROCESSING
           MOVE  COL-A-TOTAL TO RPT-A-TOTAL 
           DISPLAY RPT-A-TOTAL-ROW 
           .
       PROCESS-COL-B.
           MOVE COL-B TO COL-B-PROCESSING
           MOVE  COL-B-PROCESSING TO RPT-COL-B
           MOVE ZERO TO COL-B-TOTAL
           PERFORM PROCESS_LINE UNTIL COL-B NOT = COL-B-PROCESSING  
              OR  COL-A NOT = COL-A-PROCESSING
           MOVE  COL-B-TOTAL TO RPT-COL-TOTAL 
           DISPLAY RPT-ROW 
           MOVE SPACE TO RPT-COL-A , RPT-COL-B 
           .
       PROCESS_LINE.           
           ADD COL-COUNT TO TOTAL , COL-A-TOTAL, COL-B-TOTAL   
           PERFORM READ-LINE 
           .
       READ-LINE.
           READ INPUT-FILE 
              AT END SET END-OF-INPUT-FILE TO TRUE
           CLOSE  INPUT-FILE 
           .